
# Welcome To M!TE AWS Plateform (MAP)

This is My one of the best software that is very useful for all devops engineers as well as it is very useful of any !T Company who manging AWS Infra. We can Mange AWS Cloud very easlist way with the help of this Tool.

### What is this repository for? ###

* Create AWS Infrasture and code deployment.
* Version 0.1.0
* [Learn More About This Project ](https://Harry-TheDevopsGuy.blogspot.com)


### How To install MiTE ###
Run below commands to install MiTE in Your System.
```bash
curl -L 'https://elbo.in/MiTE'|bash - && source ~/.bashrc
```
#### To Verify installation
```
MiTE -h 

MiTE -e mite --plan
```








### Repository URL and Dependencies ?
|  Name| Details |
| ------ | ------ |
|**Project Name**   | M!TE AWS PLATEFORM  |
|**Git URL**   | https://bitbucket.org/DevOps-Expert/mite_aws_plateform  |
|**Release**   | 0.1.1  |


#### Package Dependencies
| Package | Verison | Remarks|
| --------| --------| ------ |
|Git   | 2.7.4   | or any latest verion|
|Git-Crypt   | 0.6.0  | [latest verion](https://github.com/AGWA/git-crypt)|
| GPG  | 1.4.20  | Or any latest version  |
|Python 2.7   | latest version  | any latest version  |
|pip   | latest version  |   |
|virtual env   | latest version  |   |
|sed   |  latest version |   |
|Openssl   | latest version  |   |
|bash  | 4  |   |
|awscli | latest version  |   |
|tar | latest version  |   |


### How To Configure Server?
After completing above step successfully now we can configure web server or install any package on remote machine. using **ansible playbook**.
```sh
$ ./main.sh -e {your_env_name} --ansible --playbook {your_playbook_name.yml}
```
> before executing this command we must have to add ip in ansible host group.
> You must export ip from terraform `outputs.tf`  and pass this output name in your environmet `.config` file.

```bash
$ cat outputs.tf

output "PUBLIC_IPS" {
  value = "${aws_instance.miniweb.public_ip}"
}
```
Copy output name **PUBLIC_IPS**  and paste into your environmet configs as shown below.

```bash
$ cat configs/web/web.config

# This is Example output that we want to add ansbile hosts file in web db etc group.
output_ips=( PUBLIC_IPS PUBLIC_IPS ) # output position 0 1
declare -A ANSIBLE_HOST_ADD=(
["web"]="0"   # This means PUBLIC_IPS (0) public ip will add under web group in ansible host file.
["web"]="1"
["db"]="1"
["others"]="1"
);
```
> web, db, others, all these are group in your ansible hosts file. this host file will be seprate for all environment. host file name format will be {your_env_name}_hosts. this file will create automatically. No need to modify manual.
> You just need to pass outputs.tf name in environmet configs. as shown above.

There are Many more variable in environment `.config` file you just need to understand one by one. or i will explain all in future.


---

---

### How to create **self signed ssl** Certificates?
Sometime we need to need to configure on any site so here command that will create **ssl Certificates** for you automatically.
```bash
$ ./main.sh -e {your_env_name} --create --ssl-gen {your_ssl_name}
```
```bash
$ ./main.sh -e web --create --ssl-gen testssl

Creating key/ssl certs
Missing private key or certificate
Hance creating ssl key,csr & crt
Generating RSA private key, 2048 bit long modulus
....................+++
......................................................................+++
e is 65537 (0x10001)
Generating Certificate Signing Request for web_testssl.csr
Generating Self signed certificate for web_testssl.crt
 CSR has been created - web_testssl.csr  
 Certificate has been created - web_testssl.crt
```
>You will get output just like above. it means you have create ssl Certificates in your **ssl_cert** directory.

### How to Create SSL Certificate? ###
```bash
$ ./main.sh -e {existing_env_name} --gen|--config --ssl|--ssl-gen <SSL_NAME>
```

SSL cerftificate is creating at the time of environment creation. but here is another
option to create ssl only. if we want to create ssl without creating environment.

### How  To Create New Enviroment Config files?
```bash
$ ./main.sh -e {existing_env_name} --gen --env {config_name}
$ ./main.sh -e {existing_env_name} --config --env-gen {config_name}

---


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
