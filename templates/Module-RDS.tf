# This is Auto Generated file! Don't Need to Modify Manually.

module "rds" {
  source                    = "./module/aws-rds"
  rds_username              = "${var.rds_username}"
  rds_password              = "${var.rds_password}"
  subnet_id                 = "${data.aws_subnet.default.id}"
  project                   = "${var.project}"
  rdsname                   = "${var.project}db"
  VPC_SECURITY_GROUP        = "${aws_security_group.default.id}"
  RDS_STORAGE_TYPE          = "${var.RDS_STORAGE_TYPE}"
  RDS_DB_ALLOCATED_STORAGE  = "${var.RDS_DB_ALLOCATED_STORAGE}"
  RDS_DB_MULTI_AZ           = "${var.RDS_DB_MULTI_AZ}"
  ENGINE_DBTYPE             = "${var.ENGINE_DBTYPE}"
  ENGINE_VERSION            = "${var.ENGINE_VERSION}"
  DB_INSTANCE_TYPE          = "${var.DB_INSTANCE_TYPE}"
  RDS_DB_APPLY_IMMEDIATELY  = "${var.RDS_DB_APPLY_IMMEDIATELY}"
  RDS_DB_BACKUP_WINDOW      = "${var.RDS_DB_BACKUP_WINDOW}"
  RDS_DB_MAINTENANCE_WINDOW = "${var.RDS_DB_MAINTENANCE_WINDOW}"
  BACKUP_RETENTION_PERIOD   = "${var.BACKUP_RETENTION_PERIOD}"
  RDS_DB_PORT               = "${var.RDS_DB_PORT}"
  LICENSE_MODEL             = "${var.LICENSE_MODEL}"
  owner                     = "${var.env_owner}"
  creator                   = "${var.creator}"
  requester                 = "${var.requester}"
  ENVIRONMENT_DESCRIPTION   = "${var.ENVIRONMENT_DESCRIPTION}"
  email                     = "${var.email}"
  vpc_id                    = "${data.aws_subnet.default.vpc_id}"
  ip_list                   = "${var.ip_list}"
  managed_by                = "${var.managed_by}"
}

# RDS OUTPUTS - Harry
output "rds_db_hostname" {
  value = "${module.rds.rds_db_hostname}"
}

output "rds_db_name" {
  value = "${module.rds.rds_db_name}"
}

output "rds_db_port" {
  value = "${module.rds.rds_db_port}"
}
