# This is Auto Generated file! Don't Need to Modify Manually.

module "{ENVIRONMENT}" {
  source                  = "./module/aws-s3"
  bucket_name             = "{ENVIRONMENT}"
  PLATEFORM               = "${var.PLATEFORM}"
  project                 = "${var.project}"
  owner                   = "${var.env_owner}"
  creator                 = "${var.creator}"
  requester               = "${var.requester}"
  bucket_acl              = "private"
  environment             = "{ENVIRONMENT}"
  ENVIRONMENT_DESCRIPTION = "${var.ENVIRONMENT_DESCRIPTION}"
  email                   = "${var.email}"
}

# output "{ENVIRONMENT}_ENDPOINT" {
#   value = "${module.{ENVIRONMENT}.website_endpoint}"
# }
#
# output "{ENVIRONMENT}_DOMAIN" {
#   value = "${module.{ENVIRONMENT}.website_domain}"
# }

