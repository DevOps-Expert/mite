# What Do You Want to Create?
##==================================================##

# To Create Perfect Infrasture Please Fill Carefully This file will use in script.
# Please fill only Value. don't need to add more lines.
############################################### Created by: Harry
###############################################  Email : HarryTheDevOpsGuy@Gmail.com


# Basic Details about Project:
##==================================================##
NEW_PROJECT_NAME="ABC-Web-Solution"
PROJECT_DOMAIN_NAME="HarryTheDevOpsGuy.ml"
# Available option EC2 RDS S3
AWS_SERVICES="EC2 RDS S3"

# AWS Secrets Configration:
##==================================================##
AWS_ACCOUNT_OWNER="HARRY"
YOUR_AWS_ACCESS_KEY="AWS ACCESS KEY HERE"
YOUR_AWS_SECRET_KEY="AWS SECRET KEY HERE"
YOUR_INFRA_REGION="ap-south-1"

# EC2 Instance Configration:
##==================================================##
REQUIRED_PACKAGES="nginx php7.2.21 git"
WEB_SERVER_ENVIRONMENT="nginx"
ENVIRONMENT_SETUP="dev"
INSTANCE_TYPE="t2.micro"
VOLUME_SIZE="8"

# RDS Server Configration:
##==================================================##
RDS_INSTANCE_TYPE="db.t2.micro"
RDS_DB_VOLUME_SIZE="8"
# mariadb/postgres/mysql
RDS_DB_ENGINE="mysql"
RDS_DB_ENGINE_VERSION="5.7.21"
RDS_DB_PORT="3306"
RDS_DB_NAMES="db1 db2 db3"
RDS_DB_USERS="user1 user2 user3"


# S3 Bucket Configration:
##=====================================##
S3_BUCKETS_NAME="bucket1 bucket2 bucket3"


# Environment tag Details:
##=====================================##
ENVIRONMENT_REQUESTED_BY="Prem Verma"
ENVIRONMENT_CREATED_BY="Harry The DevOps Guy"
OFFICIAL_EMAIL="HarryTheDevOpsGuy@gmail.com"
ENVIRONMENT_TICKET_ID="1112"
