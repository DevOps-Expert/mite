#!/usr/bin/env bash
INFRA_PATH="/tmp/"
INFRA_NAME="web_cloud"
ENVIRONMENTS_NAME=("kthpa" "kthpb" "ktrka" "ktrkb")

cd ${INFRA_PATH}
mkdir -p ${INFRA_NAME}/{terraform,ssh_certs,bin,tmp}
mkdir -p  ${INFRA_NAME}/run_functions/{setup_script,main,python}
mkdir -p  ${INFRA_NAME}/ansible/playbook/{assets,group_vars,roles}
mkdir -p  ${INFRA_NAME}/ansible/hosts

touch  ${INFRA_NAME}/{run_vars,main.sh}
touch  ${INFRA_NAME}/configs/{common_environment.configs,common_environment.secrets}

for ENV_NAME in ${ENVIRONMENTS_NAME[@]}
do
echo  "Creating Configs for : ${ENV_NAME}"
mkdir -p  ${INFRA_NAME}/configs/${ENV_NAME}
touch  ${INFRA_NAME}/configs/${ENV_NAME}/{${ENV_NAME}.configs,${ENV_NAME}_deploy,${ENV_NAME}.secrets}
touch  ${INFRA_NAME}/ansible/hosts/${ENV_NAME}_hosts
done

TREE=$(tree ${INFRA_NAME})
cat >${INFRA_NAME}/README.md <<EOF
${TREE}

Getting Start With - Harry

${INFRA_NAME} -  This is Name of Root Project.

EOF
