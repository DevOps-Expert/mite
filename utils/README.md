
web_cloud                             : This is The Name of Project.
├── ansible                           : This Directory will use keep all Ansible Codes, hosts, playbook, roles etc.
│   ├── hosts                         : This is Host directory This will use to Keep all hosts file. Host file can be multiple.
│   │   ├── kthpa_hosts               : This is Host file for as per environment name.
│   │   ├── kthpb_hosts               : This is Host file for as per environment name.
│   │   ├── ktrka_hosts               : This is Host file for as per environment name.
│   │   └── ktrkb_hosts               : This is Host file for as per environment name.
│   └── playbook                      : This directory will use to keep all playbooks related files. like Assets, group_vars.
│       ├── assets                    : This directory will use keep any files that will use in playbooks.
│       ├── group_vars                : This is variable Directory.
│       └── roles                     : Here we will keep all ansible Roles.
├── bin                               : This directory will use to keep Binary files that will use in this environment like "Terraform".
├── configs                           : This is the Configis Directoy That will use to Keep all Environment configs.
│   ├── common_environment.configs    : This Common Configs that will use in all Eniviroment Configs.
│   ├── common_environment.secrets    : This Common Secret files this file will hold sensitive information like Credentials.
│   ├── kthpa                         : This is one Eniviroment Directory.
│   │   ├── kithpa.secrets            : This is Environment Specific Secret file. This will hold sensitive informations.  
│   │   ├── kthpa.configs             : This is Environment Specific Configs file. This will hold sensitive informations.  
│   │   ├── kthpa_deploy              : This is Environment Specific Configs file. This will use to establish ssh connection.
│   │   └── kthpa.secrets             : This is Environment Specific Configs file. This will hold sensitive informations.
│   ├── kthpb                         
│   │   ├── kthpb.configs
│   │   ├── kthpb_deploy
│   │   └── kthpb.secrets
│   ├── ktrka
│   │   ├── ktrka.configs
│   │   ├── ktrka_deploy
│   │   └── ktrka.secrets
│   └── ktrkb
│       ├── ktrkb.configs
│       ├── ktrkb_deploy
│       └── ktrkb.secrets
├── main.sh                          : This file is main file. this will make everything work together.
├── README.md                        : README Here will breif Description about project.
├── run_functions                    : Run Function Directoy will use to keep all scripts. like python, shell, and other helping scripts.
│   ├── main
│   ├── python
│   └── setup_script
├── run_vars                         : Run Vars will use to store all initial variables most variable will be statics.
├── ssl_certs                        : This directory will use to keep all kind of ssl Certs.
├── terraform                        : This directory will use to keep all terraform code.
└── tmp                              : Tmp This is only for store files on temporarly basis.

19 directories, 22 files
