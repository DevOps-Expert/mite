#!/usr/bin/env bash
# Git Application and branch details; must be provide for app deployment.
# https://github.com/jrgp/linfo.git
#
#
export PROJECT_REPO="https://github.com/jrgp/linfo.git"
export DEPLOY_BRANCH="master"
export CLONE_DIR="WebSite"


rm -rf /var/www/html/*

cd ~/
echo "git clone ${PROJECT_REPO} ${CLONE_DIR} branch name : ${DEPLOY_BRANCH} and ${CLONE_DIR}"
git clone ${PROJECT_REPO} ${CLONE_DIR}
cd ${CLONE_DIR}
git checkout ${DEPLOY_BRANCH}


rsync -rhv * /var/www/html/


rm -rf ~/${CLONE_DIR}
mv /var/www/html/sample.config.inc.php /var/www/html/config.inc.php
