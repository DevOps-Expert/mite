#!/bin/bash
ENVIRONMENT_NAME=$1
#DELETE_ENV="false"

export INIT_NAME=$(echo ${ENVIRONMENT_NAME} | tr '[:upper:]' '[:lower:]')
CLONE_PATH="${HOME}/MiTE"
REPO_NAME="${CLONE_PATH}/MiTE-AWS-PLATEFORM"
GIT_BRANCH="update/modules"
INIT_CONFIG="${HOME}/AWS_ENVIRONMENT_CONFIGS"
CONFIG_FILE="${INIT_CONFIG}/${INIT_NAME}_initial_configs.sh"
GIT_URL="git@bitbucket.org:DevOps-Expert/mite_aws_plateform.git"
NEW_ENV_BRANCH="MiTE_Environment/${INIT_NAME}"
CONFIG_DIR="configs/${INIT_NAME}"
KEY="${HOME}"


mkdir -p ${CLONE_PATH}

git_global_config(){
git config --global user.name "Jenkins"
git config --global user.email "HarryTheDevOpsGuy@gmail.com"
}

config_files(){
  if [[ ${USER} == "jenkins" ]]; then
    if [[ -f ${WORKSPACE}/ENVIRONMENT_CONFIGS  ]]; then
      echo "Found Initial Configs for ${NEW_ENV_BRANCH}."
      mv ${WORKSPACE}/ENVIRONMENT_CONFIGS ${CONFIG_FILE}
    fi
  else
    echo "Please Checked Your Initial Configs"
  fi
}

create_env_branch(){
  echo "Discard uncommitted changes!!"
  local branch="${1}"
  git reset --hard @{u}
  git checkout ${branch}
  git pull origin ${branch}
  CHECK_REMOTE=$(git branch -r|sed 's/origin\/MiTE_Environment\///'|grep "${INIT_NAME}")
  #exit 1
  if [[ "${CHECK_REMOTE}"=="${INIT_NAME}" && ! -z ${CHECK_REMOTE} ]]; then
    echo "Branch Already Exists!!"
    git checkout ${NEW_ENV_BRANCH}
    git pull origin ${NEW_ENV_BRANCH}

  elif [[ -z ${CHECK_REMOTE} ]]; then
    echo "Please wait creating new branch : ${NEW_ENV_BRANCH}"
    git branch -D ${NEW_ENV_BRANCH}
    git checkout -b ${NEW_ENV_BRANCH}
  else
    echo "OPPS!! Something Went Wrong!!"
    exit 1
  fi

}
git_status(){
  case $1 in
    1 )
      git-crypt lock
      ;;
    0 )
      if [[ ! -f "${KEY}/.mite-aws-platform.gpg" ]]; then
        echo "OPPS!! Authontication Failed!!"
        echo "You Need To Contact to HarryTheDevOpsGuy@gmail.com To Get Your Unlock Key"
        exit 1
      else
        echo "Securty Checked!!!"
        git-crypt unlock ${KEY}/.mite-aws-platform.gpg
      fi

      ;;
  esac
}

git_commit(){
  TRACK_CHANGE="$(git status|awk '{w=$1} END{print w}')"
  TRACK_LAST="$(git status|awk 'END {print $NF}')"


  case ${TRACK_CHANGE} in
    nothing )
        if [[ ${TRACK_LAST} != "clean" ]]; then
          git add -A
          git commit -m "Added New Environment ${NEW_ENV_BRANCH}"
          git push -u origin ${NEW_ENV_BRANCH}
          echo "All Changes has been Saved : ${NEW_ENV_BRANCH}"
        else
          echo "No Changes: Code already committed"
        fi
      ;;
    no )
      echo;echo "No Changes!!"
      git status
      ;;
    * )
      echo "Something went wrong !!"
      exit 1
      ;;
  esac

  git_status "1"
}

delete_environment_code(){
  if [[ ${DELETE_ENV} == "true" ]]; then
    echo "Removing code from git"
    cd ${REPO_NAME}
    git checkout master
    git push origin --delete ${NEW_ENV_BRANCH}
    git branch -D ${NEW_ENV_BRANCH}
    #exit
    else
      echo "Delete Branch : False"
      config_files
  fi
}

environment_initialize (){

  if [[ -d "${REPO_NAME}" ]]; then
    cd ${REPO_NAME}
    git_status "0"
    create_env_branch ${GIT_BRANCH}

  else
    echo "Cloning Project for You"
    git clone ${GIT_URL} ${REPO_NAME}
    cd ${REPO_NAME}
    git_global_config
    git_status "0"

    create_env_branch ${GIT_BRANCH}
  fi

  if [[ ! -d ${CONFIG_DIR} && ${ACTION_MODE} == "plan" ]]; then
    echo "Please wait Environment is Preparing."
    ./main.sh -e ${INIT_NAME} --plan -y
    if [[ -d ${CONFIG_DIR} ]]; then
      ./main.sh -e ${INIT_NAME} --plan
    fi
  elif [[ -d ${CONFIG_DIR} && ${ACTION_MODE} == "plan" ]]; then
      echo "Enviroment Already Exists."
    ./main.sh -e ${INIT_NAME} --plan
  elif [[ -d ${CONFIG_DIR} && ${ACTION_MODE} == "apply" ]]; then
    echo "executing Apply"
    ./main.sh -e ${INIT_NAME} --apply -y
  elif [[ -d ${CONFIG_DIR} && ${ACTION_MODE} == "destroy" ]]; then
    echo "Running Code To Destroy Your Environment on AWS"
    ./main.sh -e ${INIT_NAME} --destroy -y
  elif [[ -d ${CONFIG_DIR} && ${ACTION_MODE} == "ansible" && ! -z ${PLAYBOOK}  ]]; then
    echo "running Playbook ${PLAYBOOK}"
      local env_check=$(wc -l < states/${INIT_NAME}/terraform.tfstate)
      if [[ "${env_check}" -gt "50" ]]; then
        echo "Enviroment Created"
        ./main.sh -e ${INIT_NAME} --ansible --playbook ${PLAYBOOK}.yml
      else
        echo "Please Create Environment First!!"
      fi

  else
    echo "Invalid arguments !!!"

  fi
}


###==============+=Start=+===============###

delete_environment_code
environment_initialize
git_commit

###==============+=END=+===============###

# This Software is Created By Harry
# It is opensource for Everyone You can Modify and use accourding to Your Requirement.
# Visit for any kind of support and help
# Blog     ::  www.Harry-TheDevOpsGuy.blogspot.com
# Email    :: HarryTheDevOpsGuy@gmail.com
# Skype ID :: HarryTheITExpert
# Facebook :: https://www.facebook.com/HarryTheITExpert

###====================================###
