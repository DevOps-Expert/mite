output "rds_db_hostname" {
  value = "${aws_db_instance.db_instance.address}"
}

output "rds_db_name" {
  value = "${aws_db_instance.db_instance.name}"
}

output "rds_db_port" {
  value = "${aws_db_instance.db_instance.port}"
}
