variable "rds_username" {}
variable "rds_password" {}
variable "project" {}
variable "rdsname" {}

variable "RDS_DB_ALLOCATED_STORAGE" {}
variable "RDS_DB_MULTI_AZ" {}
variable "RDS_DB_PORT" {}
variable "ENGINE_DBTYPE" {}
variable "ENGINE_VERSION" {}
variable "DB_INSTANCE_TYPE" {}
variable "RDS_DB_BACKUP_WINDOW" {}
variable "subnet_id" {}
variable "VPC_SECURITY_GROUP" {}
variable "vpc_id" {}
variable "owner" {}
variable "creator" {}
variable "requester" {}
variable "email" {}
variable "managed_by" {}
variable "ENVIRONMENT_DESCRIPTION" {}

variable "LICENSE_MODEL" {
  default = "general-public-license"
}

variable "RDS_DB_APPLY_IMMEDIATELY" {
  default = "true"
}

variable "RDS_STORAGE_TYPE" {
  default = "gp2"
}

variable "RDS_DB_MAINTENANCE_WINDOW" {
  default = ""
}

variable "BACKUP_RETENTION_PERIOD" {
  default = "8"
}

variable "ip_list" {
  description = "IP Whitelist."
  default     = []
}
