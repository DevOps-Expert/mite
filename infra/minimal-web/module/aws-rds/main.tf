# RDS SERVER : No Need to write any static values Here.

resource "aws_db_instance" "db_instance" {
  identifier                = "${lower(var.project)}-${var.ENGINE_DBTYPE}-db"
  name                      = "${var.rdsname}"
  storage_type              = "${var.RDS_STORAGE_TYPE}"
  allocated_storage         = "${var.RDS_DB_ALLOCATED_STORAGE}"
  engine                    = "${var.ENGINE_DBTYPE}"
  engine_version            = "${var.ENGINE_VERSION}"
  instance_class            = "${var.DB_INSTANCE_TYPE}"
  license_model             = "${var.LICENSE_MODEL}"
  apply_immediately         = "${var.RDS_DB_APPLY_IMMEDIATELY}"
  maintenance_window        = "${var.RDS_DB_MAINTENANCE_WINDOW}"
  username                  = "${var.rds_username}"
  password                  = "${var.rds_password}"
  final_snapshot_identifier = "${lower(var.project)}-rds-${var.ENGINE_DBTYPE}-final-snapshot"
  skip_final_snapshot       = false
  copy_tags_to_snapshot     = true
  backup_retention_period   = "${var.BACKUP_RETENTION_PERIOD}"
  backup_window             = "${var.RDS_DB_BACKUP_WINDOW}"
  publicly_accessible       = true
  port                      = "${var.RDS_DB_PORT}"

  # storage_encrypted         = true
  #multi_az = "${var.RDS_DB_MULTI_AZ}"
  vpc_security_group_ids = [
    "${var.VPC_SECURITY_GROUP}",
    "${aws_security_group.rds_sg.id}",
  ]

  tags {
    Name                    = "RDS_${upper(var.project)}_${upper(var.ENGINE_DBTYPE)}_${upper(var.ENGINE_VERSION)}"
    Owner                   = "${var.owner}"
    CreatedBy               = "${var.creator}"
    RequestedBy             = "${var.requester}"
    Email                   = "${title(var.email)}"
    Environment             = "${var.project}"
    ENVIRONMENT_DESCRIPTION = "${var.ENVIRONMENT_DESCRIPTION}"
  }
}
