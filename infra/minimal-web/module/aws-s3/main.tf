# RDS SERVER : No Need to write any static values Here.

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "${lower(var.bucket_name)}"
  acl    = "${var.bucket_acl}"

  #   policy = <<EOF
  #   {
  #   "Version":"2012-10-17",
  #   "Statement":[
  #     {
  #       "Sid":"AddPerm",
  #       "Effect":"Allow",
  #       "Principal": "*",
  #       "Action":["s3:GetObject"],
  #       "Resource":["arn:aws:s3:::${var.bucket_name}/*"]
  #     }
  #   ]
  # }
  # EOF

  tags {
    Name                    = "${var.PLATEFORM}_S3_${upper(var.project)}_${upper(var.environment)}"
    Owner                   = "${var.owner}"
    CreatedBy               = "${var.creator}"
    RequestedBy             = "${var.requester}"
    Email                   = "${title(var.email)}"
    Environment             = "${var.project}"
    ENVIRONMENT_DESCRIPTION = "${var.ENVIRONMENT_DESCRIPTION}"
  }
}
