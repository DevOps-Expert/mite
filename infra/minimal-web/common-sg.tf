# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "default" {
  name        = "${var.project}-common-sg"
  description = "Basic Security Group for Web Server - CreatedBy MITE"

  #vpc_id      = "${var.vpc_id}"
  vpc_id = "${data.aws_subnet.default.vpc_id}"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = "${var.ip_list}"
    description = "${var.managed_by}"
  }

  # HTTP access from the VPC
  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    #  cidr_blocks = ["0.0.0.0/0", "${var.vpc_cidr}"]
    cidr_blocks = ["0.0.0.0/0"]

    #cidr_blocks = ["0.0.0.0/0"]
    description = "${var.managed_by}"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "${var.managed_by}"
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "${var.managed_by}"
  }
}

# Inbound Rules
resource "aws_security_group_rule" "ingress_rules" {
  count = "${length(var.allow_inbound)}"

  type        = "ingress"
  protocol    = "tcp"
  cidr_blocks = ["${element(var.allow_in_from, count.index)}"]
  from_port   = "${element(var.allow_inbound, count.index)}"
  to_port     = "${element(var.allow_inbound, count.index)}"

  security_group_id = "${aws_security_group.default.id}"
  description       = "${var.managed_by}"
}
