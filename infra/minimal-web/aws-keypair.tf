resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key-${var.project}"
  public_key = "${file(var.public_key_file)}"
}
