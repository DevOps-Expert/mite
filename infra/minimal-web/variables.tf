variable "project" {
  default = ""
}

#
# variable "private_key" {
#   default = ""
# }

variable "env_owner" {}
variable "public_key_file" {}

# variable "vpc_cidr" {}
# variable "aws_ami_id" {}
# variable "subnet_id" {}

variable "vpc_id" {}
variable "project_name" {}

variable "default_subnet" {
  description = "This value will automatically assigned from data shource"
  default     = ""
}

variable "managed_by" {
  default = "Managed By MITE - Do Not Change Manual"
}

# This variable willl use in SG.
variable "allow_inbound" {
  default     = []
  description = "This is Managed by Mite"
}

variable "allow_in_from" {
  default = []
}

variable "ip_list" {
  description = "IP Whitelist."
  default     = []
}

variable "volume_size" {
  default = "8"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "us-east-1"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "max_instance" {
  default = "1"
}

variable "requester" {}
variable "creator" {}
variable "owner" {}
variable "email" {}

variable "ENVIRONMENT_DESCRIPTION" {
  default = "This Environment has been Created with M!TE AWS Plateform and it is Managed by HarryTheDevOpsGuy"
}

# RDS Variables

variable "rds_username" {}
variable "rds_password" {}
variable "RDS_DB_ALLOCATED_STORAGE" {}
variable "RDS_DB_MULTI_AZ" {}
variable "RDS_DB_PORT" {}
variable "ENGINE_DBTYPE" {}
variable "ENGINE_VERSION" {}
variable "DB_INSTANCE_TYPE" {}
variable "RDS_DB_BACKUP_WINDOW" {}
variable "RDS_DB_APPLY_IMMEDIATELY" {}
variable "RDS_STORAGE_TYPE" {}
variable "RDS_DB_MAINTENANCE_WINDOW" {}

variable "BACKUP_RETENTION_PERIOD" {
  default = "8"
}

variable "LICENSE_MODEL" {
  default = "general-public-license"
}

variable "PLATEFORM" {
  default = "MiTE"
}
