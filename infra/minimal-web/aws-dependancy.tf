data "aws_subnet" "default" {
  id                = "${var.default_subnet}"
  availability_zone = "${var.aws_region}b"
  vpc_id            = "${data.aws_vpc.default.id}"
}

data "aws_vpc" "default" {
  id = "${var.vpc_id}"

  #  default = true
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["*hvm-ssd/ubuntu-xenial-16.04-amd64-server-20180126*"]
  }
}
