#!/usr/bin/env bash

usage() {
  echo "#################============================#####################"
  log_success "#               Welcome To MiTE AWS PLATFORM                   #"
  echo "#################============================#####################"
  echo ""
  echo " --help         -h         - To View Help Page "
  echo " --version      -v         - To check MiTE Version"
  echo " --environment  -e         - To Define environment name"
  echo " --plan                    - To Execute terraform plan"
  echo " --apply                   - To Execute terraform apply"
  echo " --destroy                 - To destroy terraform environment from aws cloud if existing"
  echo "                -y         - Auto confirmation yes."
  echo ""
  echo ""
  echo "#################============================#####################"
  log_success "#               Developed By :  Harry                          #"
  log_success "#               Email : HarryTheDevOpsGuy@gmail.com            #"
  echo "#################============================#####################"
  exit -1
}
