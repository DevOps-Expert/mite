#!/usr/bin/env bash

test_cli_arguments() {
  if [[ $# -eq 0 ]]; then
    echo -e "${RED}FATAL: no command-line argument${NC}"
  #  usage
    exit 1
  fi
}
