init_array(){
#log_info "running ${FUNCNAME[*]}"
  declare -gA AWS_REGIONS=(
  ["ap-south-1"]="ap-south-1"
  ["eu-west-1"]="eu-west-1"
  ["eu-west-2"]="eu-west-2"
  ["eu-west-3"]="eu-west-3"
  ["ap-northeast-1"]="ap-northeast-1"
  ["ap-northeast-2"]="ap-northeast-2"
  ["sa-east-1"]="sa-east-1"
  ["ca-central-1"]="ca-central-1"
  ["ap-southeast-1"]="ap-southeast-1"
  ["ap-southeast-2"]="ap-southeast-2"
  ["eu-central-1"]="eu-central-1"
  ["us-east-1"]="us-east-1"
  ["us-east-2"]="us-east-2"
  ["us-west-1"]="us-west-1"
  ["us-west-2"]="us-west-2"
  );

  declare -gA ANSI_VARS=(
  ["VARABLE_NAME"]="Value"
  )

}
