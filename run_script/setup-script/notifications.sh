#!/usr/bin/env bash

# All kind of the log informations variables.

log_info () {
local msg=$1
echo "${msg}"
}

log_error () {
local msg=$1
echo -e "${RED} ${msg} ${NC}"
}

log_warning () {
local msg=$1
echo -e "${YELLOW} ${msg} ${NC}"
}

log_success () {
local msg=$1
echo -e "${GREEN} ${msg} ${NC}"
}

log_alert () {
local msg=$1
echo -e "${YELLOW} ${msg} ${NC}"
}
