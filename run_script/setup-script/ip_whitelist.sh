#!/usr/bin/env bash

# load_pub_ip(){
#   source ${PROJECT_ROOT}/run_script/setup-script/tempvars.sh
#   WHITE_LIST_IP[${#WHITE_LIST_IP[@]}]="${MY_PUB_IP}/32"
# }
#
# allow_my_pub_ip(){
#   if [[ -z ${MY_PUB_IP} ]]; then
#     log_alert "Please Wait..."
#     touch "${PROJECT_ROOT}/run_script/setup-script/tempvars.sh"
#     export tempvars="${PROJECT_ROOT}/run_script/setup-script/tempvars.sh"
#   echo "export MY_PUB_IP=\"$(curl -s https://checkip.amazonaws.com)\"" > ${tempvars}
#   load_pub_ip
# else
#   load_pub_ip
#   fi
# }
get_my_public_ip(){
  if [[ -z ${MY_PUB_IP} ]]; then
    log_alert " Obtaining.. Public IP Address"
    export MY_PUB_IP="$(curl -s https://checkip.amazonaws.com)"
  #   log_success " YOUR PUBLIC IP: ${MY_PUB_IP}"
  # else
  #   log_alert "YOUR PUBLIC IP : ${MY_PUB_IP}"
  fi
  COMMON_IP_WHITELIST+=("${MY_PUB_IP}/32")
  #echo IP lists: ${COMMON_IP_WHITELIST[@]}
}


ip_whitelist(){
 # ${WHITE_LIST_IP[@]}
  export TF_VAR_ip_list="$(echo $@ |awk -v RS='' -v OFS='", "' 'NF { $1 = $1; print "[\"" $0 "\"]" }')"
  # echo "Whitelist ip : ${TF_VAR_ip_list}"
}



#  This will use in Security group to open some ports for some ip.

allow_ports(){
#  allow_ports ${!ALLOW_PORTS_IN[@]}
  export TF_VAR_allow_inbound="$(echo $@ |awk -v RS='' -v OFS='", "' 'NF { $1 = $1; print "[\"" $0 "\"]" }')"
  #echo "TF_VAR_allow_inbound : ${TF_VAR_allow_inbound}"
}

allow_ports_from(){
  # allow_ports_from ${ALLOW_PORTS_IN[@]}
  export TF_VAR_allow_in_from="$(echo $@ |awk -v RS='' -v OFS='", "' 'NF { $1 = $1; print "[\"" $0 "\"]" }')"
  #echo "TF_VAR_allow_in_from : ${TF_VAR_allow_in_from}"
}
