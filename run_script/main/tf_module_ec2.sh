#!/usr/bin/env bash

update_ansible_host(){
  for i in "${INIT_ENVIRONMENT[@]}"
  do
    ENVIRONMENT=$(echo ${i} | tr  '[:lower:]' '[:upper:]')
    output_ips+=("${ENVIRONMENT}_PUB_IP")
  done
}

# create_project_environment(){
# #  log_info "running ${FUNCNAME[*]}"
#   echo "Initiating Environment for : ${PROJECT_NAME} : ${INIT_ENVIRONMENT[@]}"
#   check_env_file=( $(ls ${TF_INFRA}/${TF_DIRECTORY}/Module-EC2-*.tf 2> /dev/null) )
#
#   if [[ "${#check_env_file[@]}" != "${#INIT_ENVIRONMENT[@]}" && ${TF_MODE} == "plan"  ]]; then
#     rm -f ${TF_INFRA}/${TF_DIRECTORY}/Module-EC2-*.tf
#
#     for P_ENV in "${INIT_ENVIRONMENT[@]}"
#     do
#       ENVIRONMENT=$(echo ${P_ENV} | tr  '[:lower:]' '[:upper:]')
#       EC2_FILE_PATH="${TF_INFRA}/${TF_DIRECTORY}/Module-EC2-${PROJECT_NAME_CAPS}-${ENVIRONMENT}.tf"
#       cp "${TEMPLETES}/Module-EC2.tf" "${EC2_FILE_PATH}"
#       sed -e 's|{ENVIRONMENT}|'"${ENVIRONMENT}"'|g'     \
#         --in-place=.bak "${EC2_FILE_PATH}"
#
#       rm -rf ${EC2_FILE_PATH}.bak
#
#     done
#
#   else
#     log_alert "EC2 Module files already exists!!"
#   fi
#
#   update_ansible_host
# }

create_resource(){
  #log_info "running ${FUNCNAME[*]}"

  check_env_file=( $(ls ${TF_INFRA}/${TF_DIRECTORY}/Module-${SERVICE_CHECK}-${PROJECT_NAME_CAPS}*.tf 2> /dev/null) )
  if [[ "${#check_env_file[@]}" != "${#@}" && ${TF_MODE} == "plan"  ]]; then
    rm -rf ${TF_INFRA}/${TF_DIRECTORY}/Module-${SERVICE_CHECK}-${PROJECT_NAME_CAPS}*.tf

    for AWS_RESOURCE in ${@}
    do
      ENVIRONMENT=$(echo ${AWS_RESOURCE} | tr  '[:lower:]' '[:upper:]')
      DESTINATION_FILE_PATH="${TF_INFRA}/${TF_DIRECTORY}/Module-${SERVICE_CHECK}-${PROJECT_NAME_CAPS}-${ENVIRONMENT}.tf"
      [ -f "${DESTINATION_FILE_PATH}" ] || \
        cp -f ${TEMPLETES}/Module-${SERVICE_CHECK}.tf ${DESTINATION_FILE_PATH}
        sed -e 's|{ENVIRONMENT}|'"${ENVIRONMENT}"'|g'     \
          --in-place=.bak "${DESTINATION_FILE_PATH}"

        rm -rf ${DESTINATION_FILE_PATH}.bak
    done
  else
    log_alert "${SERVICE_CHECK} Module files already exists!!"
  fi
}




including_multiple_tf_module(){
#  log_info "running ${FUNCNAME[*]}"

  for SERVICE_CHECK in ${ALL_AWS_MODULE[@]}
  do

    if [[ "${AWS_SERVICES[@]}" =~ "${SERVICE_CHECK}" ]]; then

      case ${SERVICE_CHECK} in
        RDS )
          echo "Prepairing Code for 1 ${SERVICE_CHECK} DB Instance : RDS_DB"
          DESTINATION_FILE_PATH="${TF_INFRA}/${TF_DIRECTORY}/Module-${SERVICE_CHECK}-${PROJECT_NAME_CAPS}.tf"
          [ -f "${DESTINATION_FILE_PATH}" ] || \
            cp -f ${TEMPLETES}/Module-${SERVICE_CHECK}.tf ${DESTINATION_FILE_PATH}
          ;;
        S3 )
          echo "Prepairing Code for ${#S3_BUCKETS_NAME[@]} ${SERVICE_CHECK} Buckets  : ${S3_BUCKETS_NAME[@]}"
          create_resource ${S3_BUCKETS_NAME[@]}
          ;;

        EC2 )
          echo "Prepairing Code for ${#INIT_ENVIRONMENT[@]} ${SERVICE_CHECK} Instances  : ${INIT_ENVIRONMENT[@]}"
          #create_project_environment
          create_resource ${INIT_ENVIRONMENT[@]}
          update_ansible_host

          ;;
      esac
    else
      log_alert "Removed old ${SERVICE_CHECK} Modules"
      rm -rf ${TF_INFRA}/${TF_DIRECTORY}/Module-${SERVICE_CHECK}-${PROJECT_NAME_CAPS}*.tf
    fi

  done
}



required_tf_modules(){
  #log_info "running ${FUNCNAME[*]}"
  including_multiple_tf_module

  # for TF_MODULES in ${AWS_SERVICES[@]}
  # do
  #   log_alert "Prepairing ${TF_MODULES} for your requirements"
  #
  #   case ${TF_MODULES} in
  #     EC2|ec2 )
  #       create_project_environment
  #       ;;
  #     RDS|rds )
  #       including_rds_tf_module
  #       ;;
  #     S3|s3 )
  #       log_alert "executed S3"
  #       ;;
  #   esac
  #
  # done

}
