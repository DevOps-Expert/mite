#!/usr/bin/env bash

write_aws_creds(){
  #echo "running ${FUNCNAME}"
  #echo "aws region : ${YOUR_INFRA_REGION}"

  local default_region=$(grep "AWS_DEFAULT_REGION=" ${CONFIG_FILE} |cut -d "\"" -f2)
  YOUR_AWS_ACCESS_KEY=$(grep "AWS_ACCESS_KEY_ID" ${CONFIG_FILE} |cut -d"{" -f2|cut -d "}" -f1)
  AWS_SECRET_ACCESS_KEY=$(grep "YOUR_AWS_SECRET_KEY" ${CONFIG_FILE} |cut -d"{" -f2|cut -d "}" -f1)
  NEW_ACCOUNT_OWNER=$(grep "TF_VAR_env_owner" ${CONFIG_FILE}  |cut -d"\"" -f2|tr '[:lower:]' '[:upper:]')
  sed -e 's|'${NEW_ACCOUNT_OWNER}'|'"${TF_VAR_env_owner}"'|g' \
      -e 's|'${default_region}'|'"${YOUR_INFRA_REGION}"'|g' \
      -e 's|'${YOUR_AWS_ACCESS_KEY}'|'"${TF_VAR_env_owner}_ACCESS_KEY"'|g' \
      -e 's|'${AWS_SECRET_ACCESS_KEY}'|'"${TF_VAR_env_owner}_SECRET_KEY"'|g' \
    --in-place=.bak "${CONFIG_FILE}"
  rm "${CONFIG_FILE}.bak"
  source  ${SECRET_FILE}
  source ${CONFIG_FILE}
}


configure_aws_credentials(){
  #log_info "running ${FUNCNAME[*]}"
  [ -f "${SECRET_FILE}" ] || touch ${SECRET_FILE}
  local default_region=$(grep "AWS_DEFAULT_REGION=" ${CONFIG_FILE} |cut -d "\"" -f2)

  while [[ -z "${AWS_ACCOUNT_OWNER}" ]]
     do

       read -e -p "AWS A/c or Environment Owner Name: " AWS_ACCOUNT_OWNER
       log_alert "Updating AWS ACCOUNT : ${TF_VAR_env_owner} >> ${AWS_ACCOUNT_OWNER}"
       echo
     done
  if [[ -z ${YOUR_INFRA_REGION} ]]; then
    echo "Available Regions: ${AWS_REGIONS[@]}"
    read -e -p "Please Enter ENVIONMENT REGION: " -i "${default_region}" YOUR_INFRA_REGION
  fi

  TF_VAR_env_owner=$(echo ${AWS_ACCOUNT_OWNER} | tr '[:lower:]' '[:upper:]')
  CHECK_KEY=$(grep ${TF_VAR_env_owner}_ACCESS_KEY ${SECRET_FILE} | cut -d "\"" -f2)



  if [[ ${#CHECK_KEY} == "0" ]]; then
    if [[ -z ${YOUR_AWS_ACCESS_KEY} || -z ${YOUR_AWS_SECRET_KEY} ]]; then
    echo "Please Enter Access key and secret_key."
    read -e -p "Please Enter Access_key: " YOUR_AWS_ACCESS_KEY
    read -e -p "Please Enter secret_key: " YOUR_AWS_SECRET_KEY
    fi


    if [[ -z ${TF_VAR_env_owner} ]]; then
      log_error "Invalid Aws account Credentials"
      exit 1
    else
      if [[ "${#YOUR_AWS_ACCESS_KEY}" == "20" && "${#YOUR_AWS_SECRET_KEY}" == "40" ]]; then
        log_success "You have Provided Valid Key"
        echo "###=================AWS-${TF_VAR_env_owner}======================###" >> ${SECRET_FILE}
        echo "export ${TF_VAR_env_owner}_ACCESS_KEY=\"${YOUR_AWS_ACCESS_KEY}\"" >> ${SECRET_FILE}
        echo "export ${TF_VAR_env_owner}_SECRET_KEY=\"${YOUR_AWS_SECRET_KEY}\"" >> ${SECRET_FILE}
        # echo " export AWS_DEFAULT_REGION= us-west-2" >> ${SECRET_FILE}
        echo

        write_aws_creds
      else
        log_error "Invalid Access Key or Secret Key. Please check and Try Again."
        rm -rf ${ENV_CONFIG}
        exit 1
      fi

    fi

    source ${SECRET_FILE}
  else
    source ${SECRET_FILE}
    log_alert "AWS Key Already Exist"
    grep ${TF_VAR_env_owner}_ACCESS_KEY ${SECRET_FILE}
    write_aws_creds
  fi

  # if [[ ${#CHECK_KEY} == "20" ]]; then
  #   log_alert "AWS Key Already Exist"
  # fi
}
