#!/usr/bin/env bash

add_vars_in_ansible(){
  # Any time we can call this FUNCNAME to inject vars in ansible
  # add_vars_in_ansible "AGE" "27"

  KEY="$1"
  VALUE="$2"
  log_alert "Adding Variable in ansible = ${KEY}: ${VALUE}"
  ANSI_VARS[${KEY}]="$VALUE"
}


static_ansible_variables(){

add_vars_in_ansible "PROJECT_DOMAIN_NAME" "${PROJECT_DOMAIN}"
add_vars_in_ansible "OFFICIAL_EMAIL" "${TF_VAR_email}"
add_vars_in_ansible "WEB_ENVRIONMENT" "${WEB_ENVRIONMENT}"


}



import_shell_vars_in_ansible(){
echo "Importing Variables in ansible"
static_ansible_variables

for K in "${!ANSI_VARS[@]}";
do
   VARS_CHECK=$(cut -d"=" -f1 ${LOCAL_ENVIRONMENT_STATE_DIR}/outputs.txt|grep "$K")
  if [[ -z ${VARS_CHECK} ]]; then
    #echo "Added Vars $K = ${ANSI_VARS[$K]}"
    echo "$K = ${ANSI_VARS[$K]}" >> "${LOCAL_ENVIRONMENT_STATE_DIR}/outputs.txt"
  fi

done

}
