#!/usr/bin/env bash
TERRAFORM="${BIN_DIR}/terraform"
ANSIBLE="$(which ansible)"
ANSIBLE_PLAYBOOK_BIN="$(which ansible-playbook)"
AWS_CLI="$(which aws)"
MYSQL="$(which mysql)"
JQ=${BIN_DIR}/jq

if [[ -z ${AWS_CLI} ]]; then
  log_alert "Installing AWSCLI..."
  sudo apt install awscli -y
  AWS_CLI="$(which aws)"
fi
