#!/usr/bin/env bash

sanity_check(){
  log_info "running ${FUNCNAME[*]}"
  echo "TERRAFORM Verion: $(${TERRAFORM} -v)"
}
