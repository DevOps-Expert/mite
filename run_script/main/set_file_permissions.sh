#!/usr/bin/env bash

# single minded function.
set_ssh_key_permissions() {
  #log_info "running ${FUNCNAME[0]}"

  log_info "ENVIRONMENT: ${PROJECT_NAME}"

  chmod 600 "${TF_VAR_public_key_file}" 2> /dev/null \
    || log_error_exit "failed to set perms on ${TF_VAR_public_key_file} file"

  chmod 600 "${TF_VAR_private_key}" 2> /dev/null \
    || log_error_exit "failed to set perms on ${TF_VAR_private_key} file"

  return 0
}
